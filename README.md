1.  IP集成开发，添加ZYNQ7 Processing System，进行参数设置，关闭AXI GP0的接口，BANK1设为1.8V，打开48、49对应的UART，时钟设置为767MHz,设置DDR。
2.  自动生成外面的接口，保存。
3.  生成HDL文件，Generate Output Products。
4.  导出硬件，打开SDK，创建名为ps_timer_test的工程，选择Hello World的模板，使用到的定时器是ps7_scutimer_0，导入例程，修改TIMER_LOAD_VALUE为CPU频率的一半-1，将TimerExpired设为30，将程序写入到FPGA中，打开putty软件，端口设置为COM5，速度设置为115200，进行串口显示。
